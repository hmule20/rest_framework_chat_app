from channels.routing import ProtocolTypeRouter, URLRouter
import messaging.routing
application = ProtocolTypeRouter({
    'websocket': URLRouter(messaging.routing.websocket_urlpatterns)
})

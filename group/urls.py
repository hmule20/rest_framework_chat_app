from django.urls import path
from .views import GroupList
urlpatterns = [
    path('list/', GroupList.as_view()),
]
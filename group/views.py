from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from group.models import GroupModel, UserInGroupModel
from group.serializers import GroupSerializer


class GroupList(APIView):
    permission_classes = (AllowAny,)

    @staticmethod
    def get(request):
        user_in_group_obj = UserInGroupModel.objects.filter(user=request.user.id)
        group_list = []
        for each_group in user_in_group_obj:
            group_list.append({
                'id': each_group.group.id,
                'name': each_group.group.name,
                'is_active': each_group.group.is_active
            })
        return Response(group_list, status=status.HTTP_200_OK)
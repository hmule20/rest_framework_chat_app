from rest_framework import serializers
from .models import GroupModel, UserInGroupModel


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupModel
        fields = '__all__'

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'is_active': instance.is_active,
        }

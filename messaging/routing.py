from django.urls import path, re_path

from .consumers import ChatConsumer, UsersChatGroup, GroupChat

websocket_urlpatterns = [
    # re_path(r'ws/chat/(?P<str:username>\w+)/$', ChatConsumer),
    # re_path(r'ws/chat/(?P<username>\w+)/$', ChatConsumer),
    re_path(r'ws/test/(?P<user_group>\w+)/$', UsersChatGroup),
    re_path(r'ws/group/(?P<group>\w+)/$', GroupChat),
]
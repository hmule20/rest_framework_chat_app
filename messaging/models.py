from django.db import models

from group.models import GroupModel
from user.models import User


class MessagingModel(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sender_field")
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name="receiver_field")
    message = models.TextField()
    conversation_id = models.CharField(max_length=10, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'messaging'


class GroupMessages(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(GroupModel, on_delete=models.CASCADE, related_name='receiver_group')
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'group_messages'

from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from messaging.models import MessagingModel
from messaging.serializers import MessagingSerializer


class MessageView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, user_id):
        message_obj = MessagingModel.objects.filter(sender=user_id)
        message_srlzer = MessagingSerializer(message_obj, many=True)
        return message_srlzer.data

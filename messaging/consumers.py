from channels.generic.websocket import WebsocketConsumer
from django.contrib.auth import get_user_model
from asgiref.sync import async_to_sync
# from channels.generic.websocket import AsyncWebsocketConsumer
import json

from django.db.models import Q
from rest_framework import status
from rest_framework.response import Response

from messaging.models import MessagingModel, GroupMessages
from group.models import GroupModel, UserInGroupModel
from messaging.serializers import MessagingSerializer, GroupMessageSerializer
from messaging.views import MessageView
from user.models import User


class ChatConsumer(WebsocketConsumer):
    def fetch_messages(self, data):
        sender = data['from']
        print("sender: ", sender)
        sender_obj = User.objects.get(username=sender)
        receiver = data['to']
        receiver_obj = User.objects.get(username=receiver)
        print("receiver_obj", receiver_obj)
        messages = MessagingModel.objects.filter(Q(sender=sender_obj, receiver=receiver_obj) |
                                                 Q(sender=receiver_obj, receiver=sender_obj))
        message_srlzer = MessagingSerializer(messages, many=True)
        print("message", message_srlzer.data)
        content = {
            'messages': message_srlzer.data
        }
        self.send_message(content)

    def new_message(self, data):
        sender = data['from']
        print("sender: ", sender)
        sender_obj = User.objects.get(username=sender)
        receiver = data['to']
        receiver_obj = User.objects.get(username=receiver)
        print("receiver_obj", receiver_obj)
        message_obj = MessagingModel.objects.create(
            sender=sender_obj,
            message=data['message'],
            receiver=receiver_obj
        )
        message_srlzer = MessagingSerializer(message_obj)
        content = {
            'message': message_srlzer.data
        }
        return self.send_chat_message(json.dumps(content))

    def connect(self):
        self.username = self.scope['url_route']['kwargs']['username']
        self.room_group_name = 'chat_%s' % self.username

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()
        # self.fetch_messages(data=)

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    commands = {
        'fetch_messages': fetch_messages,
        'new_message': new_message
    }

    # Receive message from WebSocket
    def receive(self, text_data):
        # print("request.data: ", request.data)
        print("without json", text_data)
        data = json.loads(text_data)
        print("with the json: ", data['command'])
        self.commands[data['command']](self, data)

        # message = text_data_json['message']
        # if message:
        #     self.new_message(text_data_json)
        # self.fetch_messages()

        # # Send message to room group
        # async_to_sync(self.channel_layer.group_send)(
        #     self.room_group_name,
        #     {
        #         'type': 'chat_message',
        #         'message': message
        #     }
        # )

    def send_chat_message(self, message):
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    def send_message(self, message):
        print("found message: ", message)
        self.send(text_data=json.dumps(message))

    # Receive message from room group
    def chat_message(self, event):
        print('event is : ', event)
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))


class UsersChatGroup(WebsocketConsumer):
    def fetch_messages(self, data):
        sender = data['from']
        print("sender: ", sender)
        sender_obj = User.objects.get(username=sender)
        receiver = data['to']
        receiver_obj = User.objects.get(username=receiver)
        print("receiver_obj", receiver_obj)
        messages = MessagingModel.objects.filter(Q(sender=sender_obj, receiver=receiver_obj) |
                                                 Q(sender=receiver_obj, receiver=sender_obj))
        message_srlzer = MessagingSerializer(messages, many=True)
        print("message", message_srlzer.data)
        content = {
            'messages': message_srlzer.data
        }
        self.send_message(content)

    def new_message(self, data):
        sender = data['from']
        print("sender: ", sender)
        sender_obj = User.objects.get(username=sender)
        receiver = data['to']
        receiver_obj = User.objects.get(username=receiver)
        print("receiver_obj", receiver_obj)
        message_obj = MessagingModel.objects.create(
            sender=sender_obj,
            message=data['message'],
            receiver=receiver_obj
        )
        message_srlzer = MessagingSerializer(message_obj)
        content = {
            'message': message_srlzer.data
        }
        return self.send_chat_message(json.dumps(content))

    def connect(self):
        print("This is scope: ", self.scope)
        self.user_group = self.scope['url_route']['kwargs']['user_group']
        self.room_group_name = 'chat_%s' % self.user_group

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()
        # self.fetch_messages(data=)

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    commands = {
        'fetch_messages': fetch_messages,
        'new_message': new_message
    }

    # Receive message from WebSocket
    def receive(self, text_data):
        # print("request.data: ", request.data)
        print("without json", text_data)
        data = json.loads(text_data)
        print("with the json: ", data['command'])
        self.commands[data['command']](self, data)

        # message = text_data_json['message']
        # if message:
        #     self.new_message(text_data_json)
        # self.fetch_messages()

        # # Send message to room group
        # async_to_sync(self.channel_layer.group_send)(
        #     self.room_group_name,
        #     {
        #         'type': 'chat_message',
        #         'message': message
        #     }
        # )

    def send_chat_message(self, message):
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    def send_message(self, message):
        print("found message: ", message)
        self.send(text_data=json.dumps(message))

    # Receive message from room group
    def chat_message(self, event):
        print('event is : ', event)
        message = []
        message.append(json.loads(event['message'])['message'])
        print("This is message", message)
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'messages': message
        }))


class GroupChat(WebsocketConsumer):
    def fetch_messages(self, data):
        sender = data['from']
        print("sender: ", sender)
        sender_obj = User.objects.get(username=sender)
        group = data['to']
        group_obj = GroupModel.objects.get(name=group)
        try:
            user_in_obj = UserInGroupModel.objects.get(group=group_obj, user=sender_obj)
            # print("****************group***********", user_in_obj)
            print("group_obj", group_obj)
            group_messages = GroupMessages.objects.filter(group=group_obj)
            group_message_srlzer = GroupMessageSerializer(group_messages, many=True)
            # print("message", group_message_srlzer.data)
            content = {
                'messages': group_message_srlzer.data
            }
            self.send_message(content)
        except UserInGroupModel.DoesNotExist:
            self.close()
            return Response({'message': 'You arent added in this group'}, status=status.HTTP_400_BAD_REQUEST)
        except GroupModel.DoesNotExist:
            return Response({'message': 'Group Does not exist'}, status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response({'message': 'User does not exist'}, status=status.HTTP_400_BAD_REQUEST)

    def new_message(self, data):
        sender = data['from']
        print("sender: ", sender)
        sender_obj = User.objects.get(username=sender)
        group = data['to']
        group_obj = GroupModel.objects.get(name=group)
        print("group_obj", group_obj)
        try:
            UserInGroupModel.objects.get(group=group_obj, user=sender_obj)
            group_message_obj = GroupMessages.objects.create(
                sender=sender_obj,
                message=data['message'],
                group=group_obj
            )
            group_message_srlzer = GroupMessageSerializer(group_message_obj)
            content = {
                'message': group_message_srlzer.data
            }
            return self.send_chat_message(json.dumps(content))
        except UserInGroupModel.DoesNotExist:
            self.close()
            return Response({'message': 'You arent added in this group'}, status=status.HTTP_400_BAD_REQUEST)
        except GroupModel.DoesNotExist:
            return Response({'message': 'Group Does not exist'}, status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response({'message': 'User does not exist'}, status=status.HTTP_400_BAD_REQUEST)

    def connect(self):
        print("This is scope: ", self.scope)
        self.group = self.scope['url_route']['kwargs']['group']
        self.room_group_name = 'chat_%s' % self.group

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()
        # self.fetch_messages(data=)

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    commands = {
        'fetch_messages': fetch_messages,
        'new_message': new_message
    }

    # Receive message from WebSocket
    def receive(self, text_data):
        # print("request.data: ", request.data)
        print("without json", text_data)
        data = json.loads(text_data)
        print("with the json: ", data['command'])
        self.commands[data['command']](self, data)

        # message = text_data_json['message']
        # if message:
        #     self.new_message(text_data_json)
        # self.fetch_messages()

        # # Send message to room group
        # async_to_sync(self.channel_layer.group_send)(
        #     self.room_group_name,
        #     {
        #         'type': 'chat_message',
        #         'message': message
        #     }
        # )

    def send_chat_message(self, message):
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    def send_message(self, message):
        # print("found message: ", message)
        self.send(text_data=json.dumps(message))

    # Receive message from room group
    def chat_message(self, event):
        print('event is : ', event)
        message = []
        message.append(json.loads(event['message'])['message'])
        print("This is message", message)
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'messages': message
        }))

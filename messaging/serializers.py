from rest_framework import serializers
from .models import MessagingModel, GroupMessages


class MessagingSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessagingModel
        fields = '__all__'

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'sender': instance.sender.username,
            'message': instance.message,
            'receiver': instance.receiver.username,
            # 'created_at': instance.created_at
        }


class GroupMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupMessages
        fields = '__all__'

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'sender': instance.sender.username,
            'message': instance.message,
            'group': instance.group.name,
        }
